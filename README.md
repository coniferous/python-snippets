# Python snippets

Python code fragments useful for physics applications.
I'm no Python expert, but I know some tricks that I want to share.

If you have some code, share it!

If you have some suggestions, send me a message!

And most importantly good luck!
